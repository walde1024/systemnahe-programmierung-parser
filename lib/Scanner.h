/*
 * Scanner.h
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#ifndef SCANNER_H_
#define SCANNER_H_

class Buffer;
class AutomatWrapper;
class Symboltable;
class Token;
class FileWriter;

#include "LangType.h"

class Scanner {
private:

	Buffer* mBuffer;
	AutomatWrapper* mAutomat;
	Symboltable* mSymboltable;
	FileWriter* mFileWriter;

	int mBufferSize = 1024;
	char *mCurrentTokenBuffer = new char[1024];
	int mCurrentTokenBufferPosition = 0;
	//Doubles the buffer (for long lexems)
	void increaseBuffer();

	char *mCurrentBacktrackingBuffer = new char[1024];
	int mCurrentBacktrackingBufferPosition = 0;
	int mCurrentBacktrackingParametersCount;

	//Count how much bytes were from the buffer from the actual line
	int mRead = 0;

	int mLine = 1;
	int mCol = 0;

	void handleNewLine();
	Token* handleAutomat(char parameter);

	void pushToTokenBuffer(char parameter);

	Token* handleBackTrackingMode();
	Token* handleBackTrackingModeAccepted(int unprocessedSymbolsIndex);

	int getLexemIntegerValue();

	Token* createToken(LangType);

	bool addedToBuffer;

	Token* mCurrentToken;

	char mNextParameter;

	bool handleLastBufferParameterAgain;

public:
	Scanner(char* inputFile, Symboltable* tab, FileWriter* writer);
	virtual ~Scanner();

	Token* nextToken();

	void flushFileWriter();
};

#endif /* SCANNER_H_ */
