/*
 * SymbolKey.h
 *
 *  Created on: Oct 18, 2015
 *      Author: walde
 */

#ifndef SYMBOLTABLE_INCLUDES_SYMBOLKEY_H_
#define SYMBOLTABLE_INCLUDES_SYMBOLKEY_H_

class SymbolKey {
private:

	int mRow;
	int mIndex;

public:
	SymbolKey(int row, int index);
	virtual ~SymbolKey();

	int getRow();
	int getIndex();
};

#endif /* SYMBOLTABLE_INCLUDES_SYMBOLKEY_H_ */
