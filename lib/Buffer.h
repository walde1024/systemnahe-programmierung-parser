/*
 * Buffer.h
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#ifndef BUFFER_H_
#define BUFFER_H_

class Buffer {
public:
	Buffer(char *filePath);
	virtual ~Buffer();

	char getChar();
	char ungetChar();

private:

	//Handle is returned on "open" function call.
	int handle;

	//How many bytes are read on a read function call.
	int readBytes;

	void *buffer;
	char *textBuffer;

	int currentCharPointer;

	//Returns true if more than 0 char were read.
	bool readNext();
};

#endif /* BUFFER_H_ */
