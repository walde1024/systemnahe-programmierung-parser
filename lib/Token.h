/*
 * Token.h
 *
 *  Created on: Oct 18, 2015
 *      Author: walde
 */

#ifndef SCANNER_SRC_TOKEN_H_
#define SCANNER_SRC_TOKEN_H_

#include "LangType.h"
#include "SignType.h"

class SymbolKey;
class Symboltable;
class FileWriter;

class Token {
private:

	LangType mLangType;
	SymbolKey* mSymbolKey;

	//Line and column of source file
	int mLine;
	int mColumn;

	//To convert the int values while writing
	char *mLineStr;
	char *mColStr;

	//For integer tokens.
	int mValue;
	//For error tokens.
	char mErrorSymbol;
	//For sign tokens
	SignType mSignType;

	//Finds and inits the signtype if this token is a sign token
	void findOutSignType(Symboltable* table);

	void writeSignTypeToFile(FileWriter* writer);

public:
	Token(LangType type, SymbolKey* key, int line, int column, Symboltable* table);
	virtual ~Token();

	//For integer tokens
	void setValue(int value);
	int getValue();

	//For error symbols
	void setErrorSymbol(char symbol);
	char getErrorSymbol();

	void printToConsole(Symboltable* table);
	void printToFile(Symboltable* table, FileWriter* writer);

	int getLine();
	int getColumn();

	LangType getLangType();
	SignType getSignType();
	SymbolKey* getSymbolKey();
};

#endif /* SCANNER_SRC_TOKEN_H_ */
