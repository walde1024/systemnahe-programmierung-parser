/*
 * Symboltable.h
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#ifndef SYMBOLTABLE_H_
#define SYMBOLTABLE_H_

#include "Bucket.h"

class SymbolKey;

class Symboltable {
private:

	int BUFFER_SIZE = 4096;
	const int BUCKET_SIZE = 50000;

	char **mSymbolBuffer = 0;

	Bucket mBuckets[50000];

	int mRows;
	int mSymbolCount;

	//LexemSize required to check if lexem is longer than bufferSize
	void addRowToBuffer(int lexemSize);

	SymbolKey* insertIntoStringTable(char* identifier, int size);
	SymbolKey* insertIntoBucket(char* identifier, int size);

	int hash(const char* identifier);

public:
	Symboltable();
	virtual ~Symboltable();

	SymbolKey* addLexem(char* identifier, int size);
	SymbolKey* addKeyValue(char* key, char* value, int keySize, int valueSize);
	char* getKeyValue(char* key, int keySize);
	char* getLexem(SymbolKey *key);
	char* getValue(SymbolKey *key);
};

#endif /* SYMBOLTABLE_H_ */
