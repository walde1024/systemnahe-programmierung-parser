//============================================================================
// Name        : Parser.cpp
// Author      : Waldemar Schlegel
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
//============================================================================

#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include "../includes/Client.h"

int main(int argc, char **argv) {

	if (argc < 3) {
		fprintf (stderr, "\nPlease provide an input and outpufile. Example: './ScannerTest read.txt out.txt'\n");
		return EXIT_FAILURE;
	}

	Client *p = new Client(argv[1], argv[2]);
	p->parse();

	return 0;
}


