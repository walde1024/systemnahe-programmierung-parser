/*
 * Client.cpp
 *
 *  Created on: Oct 29, 2015
 *      Author: walde
 */

#include "../includes/Client.h"
#include "../includes/Parser.h"
#include "../includes/tree/ProgTree.h"
#include "../lib/Symboltable.h"
#include "../lib/FileWriter.h"

Client::Client(char *inputFile, char *outputFile) {
	mParser = new Parser(inputFile, outputFile);
	mCodeFileWriter = new FileWriter(outputFile);
}

Client::~Client() {
	delete mParser;
	delete mCodeFileWriter;
}

void Client::parse() {
	ProgTree* tree = new ProgTree();
	if (mParser->parse(tree)) {
		tree->typeCheck(mParser->getSymboltable());

		if (!tree->hasTypeCheckError()) {
			tree->makeCode(mCodeFileWriter, mParser->getSymboltable());
			mCodeFileWriter->flush();
		}
	}
}
