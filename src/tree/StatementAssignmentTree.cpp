/*
 * StatementAssignmentTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/StatementAssignmentTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/IndexTree.h"
#include "../../includes/tree/ExpTree.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"
#include "../../lib/Token.h"


StatementAssignmentTree::StatementAssignmentTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

StatementAssignmentTree::~StatementAssignmentTree() {
	// TODO Auto-generated destructor stub
}

void StatementAssignmentTree::typeCheck(Symboltable* table) {
	mIndexTree->typeCheck(table);
	mExpTree->typeCheck(table);

	char* typeValue = getTypeValue(table, mIdentifierLeaf);
	if (typeValue == '\0') {
		this->errorIdentifierNotDefined(mIdentifierLeaf->getToken());
		this->setType(errorType);
	}
	else if (mExpTree->getType() == intType && (
			(*typeValue == *mIntTypeValue && mIndexTree->getType() == emptyType)
			|| (*typeValue == *mArrayTypeValue && mIndexTree->getType() == arrayType))) {

		this->setType(noType);
	}
	else {
		this->errorIncompatibleTypes(mIdentifierLeaf->getToken());
		this->setType(errorType);
	}
}

void StatementAssignmentTree::makeCode(FileWriter* writer, Symboltable* table) {
	mExpTree->makeCode(writer, table);

	writer->writeToFile("LA $");
	writer->writeToFile(table->getLexem(mIdentifierLeaf->getToken()->getSymbolKey()));
	writeNewLine(writer);

	mIndexTree->makeCode(writer, table);

	writer->writeToFile("STR");
	writeNewLine(writer);
}

