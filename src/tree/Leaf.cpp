/*
 * Leaf.cpp
 *
 *  Created on: Nov 5, 2015
 *      Author: walde
 */

#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/Tree.h"
#include "../../lib/Token.h"
#include "../../lib/Symboltable.h"

Leaf::Leaf(Token* token): Tree(noType) {
	mToken = token;
}

Leaf::~Leaf() {
	// TODO Auto-generated destructor stub
}

Token* Leaf::getToken() {
	return mToken;
}

void Leaf::typeCheck(Symboltable* table) {

}

void Leaf::makeCode(FileWriter* writer, Symboltable* table) {

}
