/*
 * StatementStatementsTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/StatementStatementsTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/StatementsTree.h"
#include "../../lib/Symboltable.h"

StatementStatementsTree::StatementStatementsTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

StatementStatementsTree::~StatementStatementsTree() {
	// TODO Auto-generated destructor stub
}

void StatementStatementsTree::typeCheck(Symboltable* table) {
	mStatementsTree->typeCheck(table);
	this->setType(noType);
}

void StatementStatementsTree::makeCode(FileWriter* writer, Symboltable* table) {
	mStatementsTree->makeCode(writer, table);
}
