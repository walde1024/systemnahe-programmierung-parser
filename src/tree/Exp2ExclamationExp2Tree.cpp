/*
 * Exp2ExclamationExp2Tree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/Exp2ExclamationExp2Tree.h"
#include "../../includes/tree/Leaf.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"

Exp2ExclamationExp2Tree::Exp2ExclamationExp2Tree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

Exp2ExclamationExp2Tree::~Exp2ExclamationExp2Tree() {
	// TODO Auto-generated destructor stub
}

void Exp2ExclamationExp2Tree::typeCheck(Symboltable* table) {
	mExp2Tree->typeCheck(table);
	if (mExp2Tree->getType() != intType) {
		this->setType(errorType);
	}
	else {
		this->setType(intType);
	}
}

void Exp2ExclamationExp2Tree::makeCode(FileWriter* writer, Symboltable* table) {
	mExp2Tree->makeCode(writer, table);
	writer->writeToFile("NOT");
	writeNewLine(writer);
}
