/*
 * StatementWriteTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/StatementWriteTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/ExpTree.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"


StatementWriteTree::StatementWriteTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

StatementWriteTree::~StatementWriteTree() {
	// TODO Auto-generated destructor stub
}

void StatementWriteTree::typeCheck(Symboltable* table) {
	mExpTree->typeCheck(table);
	this->setType(noType);
}

void StatementWriteTree::makeCode(FileWriter* writer, Symboltable* table) {
	mExpTree->makeCode(writer, table);
	writer->writeToFile("PRI");
	writeNewLine(writer);
}
