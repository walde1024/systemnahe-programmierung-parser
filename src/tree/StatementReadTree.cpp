/*
 * StatementReadTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/StatementReadTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/IndexTree.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"
#include "../../lib/Token.h"

StatementReadTree::StatementReadTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

StatementReadTree::~StatementReadTree() {
	// TODO Auto-generated destructor stub
}

void StatementReadTree::typeCheck(Symboltable* table) {
	mIndexTree->typeCheck(table);

	if (getTypeValue(table, mIdentifierLeaf) == '\0') {
		errorIdentifierNotDefined(mIdentifierLeaf->getToken());
		this->setType(errorType);
	}
	else if ( ((*(getTypeValue(table, mIdentifierLeaf)) == *mIntTypeValue) && mIndexTree->getType() == emptyType)
			|| ((*(getTypeValue(table, mIdentifierLeaf)) == *mArrayTypeValue) && mIndexTree->getType() == arrayType)) {

		this->setType(noType);
	}
	else {
		errorIncompatibleTypes(mIdentifierLeaf->getToken());
	}
}

void StatementReadTree::makeCode(FileWriter* writer, Symboltable* table) {
	writer->writeToFile("REA");
	writeNewLine(writer);

	writer->writeToFile("LA $");
	writer->writeToFile(table->getLexem(mIdentifierLeaf->getToken()->getSymbolKey()));
	writeNewLine(writer);
	mIndexTree->makeCode(writer, table);

	writer->writeToFile("STR");
	writeNewLine(writer);
}











