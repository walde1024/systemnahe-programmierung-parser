/*
 * StatementsTree.cpp
 *
 *  Created on: Nov 6, 2015
 *      Author: walde
 */

#include "../../includes/tree/StatementsTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"


StatementsTree::StatementsTree(): Tree(noType) {

}

StatementsTree::~StatementsTree() {
	// TODO Auto-generated destructor stub
}

void StatementsTree::typeCheck(Symboltable* table) {
	if (mStatementTree && mStatementsTree) {
		mStatementTree->typeCheck(table);
		mStatementsTree->typeCheck(table);

		this->setType(noType);
	}
	else {
		this->setType(emptyType);
	}
}

void StatementsTree::makeCode(FileWriter* writer, Symboltable* table) {
	if (this->getType() == emptyType) {
		writer->writeToFile("NOP");
		this->writeNewLine(writer);
	}
	else {
		mStatementTree->makeCode(writer, table);
		mStatementsTree->makeCode(writer, table);
	}
}











