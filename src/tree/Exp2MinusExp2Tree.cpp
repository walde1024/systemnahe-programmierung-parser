/*
 * Exp2MinusExp2Tree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/Exp2MinusExp2Tree.h"
#include "../../includes/tree/Leaf.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"

Exp2MinusExp2Tree::Exp2MinusExp2Tree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

Exp2MinusExp2Tree::~Exp2MinusExp2Tree() {
	// TODO Auto-generated destructor stub
}

void Exp2MinusExp2Tree::typeCheck(Symboltable* table) {
	mExp2Tree->typeCheck(table);
	this->setType(mExp2Tree->getType());
}

void Exp2MinusExp2Tree::makeCode(FileWriter* writer, Symboltable* table) {
	writer->writeToFile("LC 0");
	writeNewLine(writer);
	mExp2Tree->makeCode(writer, table);
	writer->writeToFile("SUB");
	writeNewLine(writer);
}

