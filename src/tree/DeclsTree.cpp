/*
 * DeclsTree.cpp
 *
 *  Created on: Nov 6, 2015
 *      Author: walde
 */

#include "../../includes/tree/DeclsTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/DeclTree.h"
#include "../../lib/Symboltable.h"

DeclsTree::DeclsTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

DeclsTree::~DeclsTree() {
	// TODO Auto-generated destructor stub
}

void DeclsTree::typeCheck(Symboltable* table) {
	if (mDeclTree && mDeclsTree) {
		mDeclTree->typeCheck(table);
		mDeclsTree->typeCheck(table);
	}

	this->setType(noType);
}

void DeclsTree::makeCode(FileWriter* writer, Symboltable* table) {
	if (mDeclTree && mDeclsTree) {
		mDeclTree->makeCode(writer, table);
		mDeclsTree->makeCode(writer, table);
	}
}
