/*
 * ProgTree.cpp
 *
 *  Created on: Nov 6, 2015
 *      Author: walde
 */

#include "../../includes/tree/ProgTree.h"
#include "../../includes/tree/DeclsTree.h"
#include "../../includes/tree/StatementsTree.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"
#include <iostream>

ProgTree::ProgTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

ProgTree::~ProgTree() {
	// TODO Auto-generated destructor stub
}

void ProgTree::typeCheck(Symboltable* table) {
	std::cout << "Type checking...\n";

	mDeclsTree->typeCheck(table);
	mStatementsTree->typeCheck(table);

	this->setType(noType);
}

void ProgTree::makeCode(FileWriter* writer, Symboltable* table) {
	std::cout << "Generate code...\n";

	mDeclsTree->makeCode(writer, table);
	mStatementsTree->makeCode(writer, table);

	writer->writeToFile("STP");
}










