/*
 * Exp2Identifier.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/Exp2IdentifierTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/IndexTree.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"
#include "../../lib/Token.h"
#include "../../includes/tree/Leaf.h"

Exp2IdentifierTree::Exp2IdentifierTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

Exp2IdentifierTree::~Exp2IdentifierTree() {
	// TODO Auto-generated destructor stub
}

void Exp2IdentifierTree::typeCheck(Symboltable* table) {
	mIndexTree->typeCheck(table);

	if (getTypeValue(table, mIdentifierLeaf) == '\0') {
		errorIdentifierNotDefined(mIdentifierLeaf->getToken());
		this->setType(errorType);
	}
	else if (*(getTypeValue(table, mIdentifierLeaf)) == *mIntTypeValue && mIndexTree->getType() == emptyType) {
		if (*(getTypeValue(table, mIdentifierLeaf)) == *mIntTypeValue)
			this->setType(intType);
		else
			this->setType(arrayType);
	}
	else if (*(getTypeValue(table, mIdentifierLeaf)) == *mArrayTypeValue && mIndexTree->getType() == arrayType) {
		this->setType(intType);
	}
	else {
		this->errorNoPrimitiveType(mIdentifierLeaf->getToken());
		this->setType(errorType);
	}
}

void Exp2IdentifierTree::makeCode(FileWriter* writer, Symboltable* table) {
	writer->writeToFile("LA $");
	writer->writeToFile(table->getLexem(mIdentifierLeaf->getToken()->getSymbolKey()));
	writeNewLine(writer);

	mIndexTree->makeCode(writer, table);

	writer->writeToFile("LV");
	writeNewLine(writer);
}























