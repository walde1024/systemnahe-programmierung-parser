/*
 * Exp2BracketTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/Exp2BracketTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/ExpTree.h"
#include "../../lib/Symboltable.h"


Exp2BracketTree::Exp2BracketTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

Exp2BracketTree::~Exp2BracketTree() {
	// TODO Auto-generated destructor stub
}

void Exp2BracketTree::typeCheck(Symboltable* table) {
	mExpTree->typeCheck(table);
	this->setType(mExpTree->getType());
}

void Exp2BracketTree::makeCode(FileWriter* writer, Symboltable* table) {
	mExpTree->makeCode(writer, table);
}
