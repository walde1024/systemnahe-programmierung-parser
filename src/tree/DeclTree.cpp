/*
 * DeclTree.cpp
 *
 *  Created on: Nov 6, 2015
 *      Author: walde
 */

#include "../../includes/tree/DeclTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/ArrayTree.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"
#include "../../lib/Token.h"



DeclTree::DeclTree(): Tree(noType) {
	// TODO Auto-generated constructor stub
}

DeclTree::~DeclTree() {
	// TODO Auto-generated destructor stub
}

void DeclTree::typeCheck(Symboltable* table) {
	mArrayTree->typeCheck(table);

	if (mArrayTree->getType() == errorType) {
		this->setType(errorType);
		return;
	}

	char* typeValue = getTypeValue(table, mIdentifierLeaf);
	if (typeValue != '\0') {
		errorIdentifierAlreadyDefined(mIdentifierLeaf->getToken());
		this->setType(errorType);

		return;
	}
	else {
		char *typeKey = 0;
		int len = createTypeKey(&typeKey, mIdentifierLeaf, table);

		if (mArrayTree->getType() == emptyType)
			table->addKeyValue(typeKey, mIntTypeValue, len, 1);
		else if (mArrayTree->getType() == arrayType)
			table->addKeyValue(typeKey, mArrayTypeValue, len, 1);
	}
}

void DeclTree::makeCode(FileWriter* writer, Symboltable* table) {
	writer->writeToFile("DS $");
	writer->writeToFile(table->getLexem(mIdentifierLeaf->getToken()->getSymbolKey()));
	writer->writeToFile(" ");
	mArrayTree->makeCode(writer, table);

	writeNewLine(writer);
}

