/*
 * Exp2Integer.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/Exp2IntegerTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"
#include "../../includes/tree/Leaf.h"
#include "../../lib/Token.h"

Exp2IntegerTree::Exp2IntegerTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

Exp2IntegerTree::~Exp2IntegerTree() {
	// TODO Auto-generated destructor stub
}

void Exp2IntegerTree::typeCheck(Symboltable* table) {
	this->setType(intType);
}

void Exp2IntegerTree::makeCode(FileWriter* writer, Symboltable* table) {
	writer->writeToFile("LC ");
	writer->writeToFile(parseIntToCharPointer(mIntegerLeaf->getToken()->getValue()));
	writeNewLine(writer);
}
