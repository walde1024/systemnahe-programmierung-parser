/*
 * IndexTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/IndexTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/ExpTree.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"

IndexTree::IndexTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

IndexTree::~IndexTree() {
	// TODO Auto-generated destructor stub
}

void IndexTree::typeCheck(Symboltable* table) {
	if (mLeftCurlyBracketLeaf && mExpTree && mRightCurlyBracketLeaf) {
		mExpTree->typeCheck(table);
		if (mExpTree->getType() == errorType) {
			this->setType(errorType);
		}
		else {
			this->setType(arrayType);
		}
	}
	else {
		this->setType(emptyType);
	}
}

void IndexTree::makeCode(FileWriter* writer, Symboltable* table) {
	if (getType() == emptyType) {

	}
	else {
		mExpTree->makeCode(writer, table);
		writer->writeToFile("ADD");
		writeNewLine(writer);
	}
}

