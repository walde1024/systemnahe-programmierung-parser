/*
 * OpTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/OpTree.h"
#include "../../lib/Symboltable.h"
#include "../../lib/Token.h"
#include "../../includes/tree/Leaf.h"
#include "../../lib/FileWriter.h"

OpTree::OpTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

OpTree::~OpTree() {
	// TODO Auto-generated destructor stub
}

void OpTree::typeCheck(Symboltable* table) {
	if (mOperatorLeaf->getToken()->getSignType() == PLUS) {
		this->setType(opPlus);
	}
	else if (mOperatorLeaf->getToken()->getSignType() == MINUS) {
		this->setType(opMinus);
	}
	else if (mOperatorLeaf->getToken()->getSignType() == STAR) {
		this->setType(opMult);
	}
	else if (mOperatorLeaf->getToken()->getSignType() == COLON) {
		this->setType(opDiv);
	}
	else if (mOperatorLeaf->getToken()->getSignType() == LEFT_ARROW) {
		this->setType(opLess);
	}
	else if (mOperatorLeaf->getToken()->getSignType() == RIGHT_ARROW) {
		this->setType(opGreater);
	}
	else if (mOperatorLeaf->getToken()->getSignType() == EQUALS) {
		this->setType(opEqual);
	}
	else if (mOperatorLeaf->getToken()->getSignType() == LEFT_ARROW_COLON_RIGHT_ARROW) {
		this->setType(opUnEqual);
	}
	else if (mOperatorLeaf->getToken()->getSignType() == AND) {
		this->setType(opAnd);
	}
}

void OpTree::makeCode(FileWriter* writer, Symboltable* table) {
	if (this->getType() == opPlus) {
		writer->writeToFile("ADD");
		writeNewLine(writer);
	}
	else if (this->getType() == opMinus) {
		writer->writeToFile("SUB");
		writeNewLine(writer);
	}
	else if (this->getType() == opMult) {
		writer->writeToFile("MUL");
		writeNewLine(writer);
	}
	else if (this->getType() == opDiv) {
		writer->writeToFile("DIV");
			writeNewLine(writer);
	}
	else if (this->getType() == opLess) {
		writer->writeToFile("LES");
		writeNewLine(writer);
	}
	else if (this->getType() == opGreater) {

	}
	else if (this->getType() == opEqual) {
		writer->writeToFile("EQU");
			writeNewLine(writer);
	}
	else if (this->getType() == opUnEqual) {
		writer->writeToFile("EQU");
			writeNewLine(writer);
	}
	else if (this->getType() == opAnd) {
		writer->writeToFile("AND");
			writeNewLine(writer);
	}
}





























