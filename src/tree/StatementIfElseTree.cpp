/*
 * StatementIfElseTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/StatementIfElseTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/ExpTree.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"



StatementIfElseTree::StatementIfElseTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

StatementIfElseTree::~StatementIfElseTree() {
	// TODO Auto-generated destructor stub
}

void StatementIfElseTree::typeCheck(Symboltable* table) {
	mExpTree->typeCheck(table);
	mIfStatementTree->typeCheck(table);
	mElseStatementTree->typeCheck(table);

	if (mExpTree->getType() == errorType) {
		this->setType(errorType);
	}
	else {
		this->setType(noType);
	}
}

void StatementIfElseTree::makeCode(FileWriter* writer, Symboltable* table) {
	int label1 = getCurrentLabelNumber();
	int label2 = getCurrentLabelNumber();

	mExpTree->makeCode(writer, table);

	writer->writeToFile("JIN #label");
	writer->writeToFile(parseIntToCharPointer(label1));
	writeNewLine(writer);

	mIfStatementTree->makeCode(writer, table);

	writer->writeToFile("JMP #label");
	writer->writeToFile(parseIntToCharPointer(label2));
	writeNewLine(writer);

	writer->writeToFile("#label");
	writer->writeToFile(parseIntToCharPointer(label1));
	writer->writeToFile(" NOP");
	writeNewLine(writer);

	mElseStatementTree->makeCode(writer, table);

	writer->writeToFile("#label");
	writer->writeToFile(parseIntToCharPointer(label2));
	writer->writeToFile(" NOP");
	writeNewLine(writer);
}
















