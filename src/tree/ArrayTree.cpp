/*
 * ArrayTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/ArrayTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../lib/Symboltable.h"
#include "../../lib/Token.h"
#include "../../lib/FileWriter.h"

#include <stdio.h>



ArrayTree::ArrayTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

ArrayTree::~ArrayTree() {
	// TODO Auto-generated destructor stub
}

void ArrayTree::typeCheck(Symboltable* table) {
	if (mIntegerLeaf && mLeftSquareBracketLeaf && mRightSquareBracketLeaf) {
		if (mIntegerLeaf->getToken()->getValue() > 0) {
			this->setType(arrayType);
		}
		else {
			this->errorArraySizeIsZero(mIntegerLeaf->getToken());
		}
	}
	else {
		this->setType(emptyType);
	}
}

void ArrayTree::makeCode(FileWriter* writer, Symboltable* table) {
	if (this->getType() == arrayType) {
		int value = mIntegerLeaf->getToken()->getValue();
		writer->writeToFile(parseIntToCharPointer(value));
	}
	else {
		writer->writeToFile("1");
	}
}

