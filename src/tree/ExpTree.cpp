/*
 * ExpTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/ExpTree.h"
#include "../../includes/tree/OpExpTree.h"
#include "../../includes/tree/OpTree.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"



ExpTree::ExpTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

ExpTree::~ExpTree() {
	// TODO Auto-generated destructor stub
}

void ExpTree::typeCheck(Symboltable* table) {
	mExp2Tree->typeCheck(table);
	mOp_ExpTree->typeCheck(table);

	if (mOp_ExpTree->getType() == emptyType) {
		this->setType(mExp2Tree->getType());
	}
	else if (mExp2Tree->getType() != mOp_ExpTree->getType()) {
		this->setType(errorType);
	}
	else {
		this->setType(mExp2Tree->getType());
	}
}

void ExpTree::makeCode(FileWriter* writer, Symboltable* table) {
	if (mOp_ExpTree->getType() == emptyType) {
		mExp2Tree->makeCode(writer, table);
	}
	else if (mOp_ExpTree->mOpTree->getType() == opGreater) {
		mOp_ExpTree->makeCode(writer, table);
		mExp2Tree->makeCode(writer, table);
		writer->writeToFile("LES");
		writeNewLine(writer);
	}
	else if (mOp_ExpTree->mOpTree->getType() == opUnEqual) {
		mExp2Tree->makeCode(writer, table);
		mOp_ExpTree->makeCode(writer, table);
		writer->writeToFile("NOT");
		writeNewLine(writer);
	}
	else {
		mExp2Tree->makeCode(writer, table);
		mOp_ExpTree->makeCode(writer, table);
	}
}

