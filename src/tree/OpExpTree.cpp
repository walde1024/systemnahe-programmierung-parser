/*
 * OpExpTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/OpExpTree.h"
#include "../../includes/tree/ExpTree.h"
#include "../../lib/Symboltable.h"
#include "../../includes/tree/OpTree.h"
#include "../../lib/FileWriter.h"

OpExpTree::OpExpTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

OpExpTree::~OpExpTree() {
	// TODO Auto-generated destructor stub
}

void OpExpTree::typeCheck(Symboltable* table) {
	if (mOpTree && mExpTree) {
		mOpTree->typeCheck(table);
		mExpTree->typeCheck(table);

		setType(mExpTree->getType());
	}
	else {
		setType(emptyType);
	}
}

void OpExpTree::makeCode(FileWriter* writer, Symboltable* table) {
	if (this->getType() != emptyType) {
		mExpTree->makeCode(writer, table);
		mOpTree->makeCode(writer, table);
	}
}
