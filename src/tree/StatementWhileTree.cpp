/*
 * StatementWhileTree.cpp
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#include "../../includes/tree/StatementWhileTree.h"
#include "../../includes/tree/Leaf.h"
#include "../../includes/tree/ExpTree.h"
#include "../../lib/Symboltable.h"
#include "../../lib/FileWriter.h"


StatementWhileTree::StatementWhileTree(): Tree(noType) {
	// TODO Auto-generated constructor stub

}

StatementWhileTree::~StatementWhileTree() {
	// TODO Auto-generated destructor stub
}

void StatementWhileTree::typeCheck(Symboltable* table) {
	mExpTree->typeCheck(table);
	mStatementTree->typeCheck(table);

	if (mExpTree->getType() == errorType) {
		this->setType(errorType);
	}
	else {
		this->setType(noType);
	}
}

void StatementWhileTree::makeCode(FileWriter* writer, Symboltable* table) {
	int label1 = getCurrentLabelNumber();
	int label2 = getCurrentLabelNumber();

	writer->writeToFile("#label");
	writer->writeToFile(parseIntToCharPointer(label1));
	writer->writeToFile(" NOP");
	writeNewLine(writer);

	mExpTree->makeCode(writer, table);

	writer->writeToFile("JIN #label");
	writer->writeToFile(parseIntToCharPointer(label2));
	writeNewLine(writer);

	mStatementTree->makeCode(writer, table);

	writer->writeToFile("JMP #label");
	writer->writeToFile(parseIntToCharPointer(label1));
	writeNewLine(writer);

	writer->writeToFile("#label");
	writer->writeToFile(parseIntToCharPointer(label2));
	writer->writeToFile(" NOP");
	writeNewLine(writer);
}

