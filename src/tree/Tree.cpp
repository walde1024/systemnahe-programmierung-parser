/*
 * Tree.cpp
 *
 *  Created on: Nov 5, 2015
 *      Author: walde
 */

#include "../../includes/tree/Tree.h"
#include "../../includes/tree/Leaf.h"
#include "../../lib/Symboltable.h"
#include "../../lib/Token.h"
#include "../../lib/FileWriter.h"

#include <stdio.h>
#include <iostream>
#include <cstdlib>

Tree::Tree(Type type) {
	mType = noType;
	mCharBuffer = new char[15];
}

int Tree::mCurrentLabelNumber = 0;
bool Tree::mHasTypeCheckError = false;

Tree::~Tree() {
	delete mIntTypeValue;
	delete mArrayTypeValue;
	delete mCharBuffer;
}

int Tree::getCurrentLabelNumber() {
	return ++Tree::mCurrentLabelNumber;
}

void Tree::error(char message) {
	fprintf (stderr, &message);
}

Type Tree::getType() {
	return mType;
}

void Tree::setType(Type type) {
	mType = type;
}

char* Tree::getTypeValue(Symboltable* table, Leaf* leaf) {
	char *typeKey = 0;
	int len = createTypeKey(&typeKey, leaf, table);

	return table->getKeyValue(typeKey, len);
}

int Tree::createTypeKey(char** key, Leaf* leaf, Symboltable* table) {
	//create key with the following pattern: "type<lexem>"

	SymbolKey* identKey = leaf->getToken()->getSymbolKey();
	char *lexem = table->getLexem(identKey);

	//coutLexemLength. Start with 4 because 'type' has length 4
	int lexemLength = 4;
	char *lexemCount = lexem;
	while(*lexemCount) {
		lexemCount++;
		lexemLength++;
	}

	char *identTypeKey = new char[lexemLength];
	identTypeKey[0] = 't';
	identTypeKey[1] = 'y';
	identTypeKey[2] = 'p';
	identTypeKey[3] = 'e';
	for (int i = 0; i < lexemLength; i++) {
		*(identTypeKey+i+4) = *(lexem+i);
	}

	*key = identTypeKey;

	return lexemLength;
}

void Tree::errorIdentifierAlreadyDefined(Token* token) {
	mHasTypeCheckError = true;
	fprintf (stderr, "\nError: Identifier already defined.\t");
	errorPrintToken(token);
}

void Tree::errorArraySizeIsZero(Token* token) {
	mHasTypeCheckError = true;
	fprintf (stderr, "\nError: Array size cannot be 0.\t\t");
	errorPrintToken(token);
}

void Tree::errorIdentifierNotDefined(Token* token) {
	mHasTypeCheckError = true;
	fprintf (stderr, "\nError: Identifier not defined.\t\t");
	errorPrintToken(token);
}

void Tree::errorIncompatibleTypes(Token* token) {
	mHasTypeCheckError = true;
	fprintf (stderr, "\nError: Incompatible types.\t\t");
	errorPrintToken(token);
}

void Tree::errorNoPrimitiveType(Token* token) {
	mHasTypeCheckError = true;
	fprintf (stderr, "\nError: No primitive type.\t\t");
	errorPrintToken(token);
}

void Tree::errorPrintToken(Token* token) {
	fprintf (stderr, "Line: %d Column: %d\n", token->getLine(), token->getColumn());
}

bool Tree::hasTypeCheckError() {
	return mHasTypeCheckError;
}

void Tree::writeNewLine(FileWriter* writer) {
	writer->writeToFile("\n");
}

char* Tree::parseIntToCharPointer(int value) {
	sprintf(mCharBuffer, "%d", value);
	return mCharBuffer;
}
