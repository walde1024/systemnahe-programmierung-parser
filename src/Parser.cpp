/*
 * Parser.cpp
 *
 *  Created on: Oct 23, 2015
 *      Author: walde
 */

#include "../includes/Parser.h"
#include "../lib/Scanner.h"
#include "../lib/Symboltable.h"
#include "../lib/FileWriter.h"
#include "../lib/Token.h"
#include "../lib/LangType.h"
#include "../lib/SignType.h"
#include <error.h>
#include <stdio.h>
#include <iostream>

#include "../includes/tree/ArrayTree.h"
#include "../includes/tree/DeclsTree.h"
#include "../includes/tree/DeclTree.h"
#include "../includes/tree/Exp2BracketTree.h"
#include "../includes/tree/Exp2ExclamationExp2Tree.h"
#include "../includes/tree/Exp2IdentifierTree.h"
#include "../includes/tree/Exp2IntegerTree.h"
#include "../includes/tree/Exp2MinusExp2Tree.h"
#include "../includes/tree/ExpTree.h"
#include "../includes/tree/IndexTree.h"
#include "../includes/tree/OpExpTree.h"
#include "../includes/tree/Leaf.h"
#include "../includes/tree/OpTree.h"
#include "../includes/tree/ProgTree.h"
#include "../includes/tree/Tree.h"
#include "../includes/tree/StatementAssignmentTree.h"
#include "../includes/tree/StatementIfElseTree.h"
#include "../includes/tree/StatementReadTree.h"
#include "../includes/tree/StatementStatementsTree.h"
#include "../includes/tree/StatementsTree.h"
#include "../includes/tree/StatementWhileTree.h"
#include "../includes/tree/StatementWriteTree.h"

Parser::Parser(char *inputFile, char *outputFile) {

	mTable = new Symboltable();
	mWriter = new FileWriter(outputFile);

	mScanner = new Scanner(inputFile, mTable, mWriter);

	mToken = 0;
	mLastToken = 0;

	mErrorWhileParsing = false;
}

Parser::~Parser() {
	delete mScanner;
}

Symboltable* Parser::getSymboltable() {
	return mTable;
}

void Parser::error() {
	if (!mErrorWhileParsing) {
		mErrorWhileParsing = true;
		fprintf (stderr, "Syntax Error at \tLine: %d Column: %d\n", mLastToken->getLine(), mLastToken->getColumn());
	}
}

bool Parser::parse(ProgTree* tree) {
	std::cout << "Parsing...\n";

	PROG(tree);
	return !mErrorWhileParsing;
}

void Parser::PROG(ProgTree* progTree) {
	nextToken();

	progTree->mDeclsTree = new DeclsTree();
	progTree->mStatementsTree = new StatementsTree();

	DECLS(progTree->mDeclsTree);
	STATEMENTS(progTree->mStatementsTree);
}

void Parser::DECLS(DeclsTree* declsTree) {

	//int
	if (mToken->getLangType() == INT_KEYWORD) {

		declsTree->mDeclTree = new DeclTree();
		DECL(declsTree->mDeclTree);

		if (mToken->getSignType() == SEMICOLON) {
			declsTree->mSemiliconLeaf = new Leaf(mToken);
			nextToken();

			//int
			if (mToken->getLangType() == INT_KEYWORD && !mErrorWhileParsing) {
				declsTree->mDeclsTree = new DeclsTree();
				DECLS(declsTree->mDeclsTree);
			}
			else {
				declsTree->mDeclsTree = new DeclsTree();
			}
		}
		else {
			error();
		}
	}
}

void Parser::DECL(DeclTree* declTree) {
	//int
	if (mToken->getLangType() == INT_KEYWORD) {
		declTree->mIntLeaf = new Leaf(mToken);
		nextToken();

		//int [
		if (mToken->getSignType() == SQUARE_BRACKET_LEFT) {
			declTree->mArrayTree = new ArrayTree();
			ARRAY(declTree->mArrayTree);
		}
		else {
			declTree->mArrayTree = new ArrayTree();
		}
		//int ARRAY | e identifier
		if (mToken->getLangType() == IDENTIFIER) {
			declTree->mIdentifierLeaf = new Leaf(mToken);
			nextToken();
		}
		else {
			error();
		}
	}
}

void Parser::ARRAY(ArrayTree* arrayTree) {
	//int [
	if (mToken->getSignType() == SQUARE_BRACKET_LEFT) {
		arrayTree->mLeftSquareBracketLeaf = new Leaf(mToken);
		nextToken();

		//int [10
		if (mToken->getLangType() == INTEGER) {
			arrayTree->mIntegerLeaf = new Leaf(mToken);
			nextToken();

			//int [10]
			if (mToken->getSignType() == SQUARE_BRACKET_RIGHT) {
				arrayTree->mRightSquareBracketLeaf = new Leaf(mToken);
				nextToken();
			}
			else {
				error();
			}
		}
		else {
			error();
		}
	}
}

void Parser::STATEMENTS(StatementsTree* statementsTree) {
	if (mErrorWhileParsing) {
		return;
	}

	//identifier INDEX := EXP;
	if (mToken->getLangType() == IDENTIFIER) {
		statementsTree->mStatementTree = new StatementAssignmentTree();
		STATEMENT(statementsTree->mStatementTree);
		SEMICOLONSTM(&(statementsTree->mSemicolonLeaf));
	}
	//write( EXP );
	else if (mToken->getLangType() == WRITE) {
		statementsTree->mStatementTree = new StatementWriteTree();
		STATEMENT(statementsTree->mStatementTree);
		SEMICOLONSTM(&(statementsTree->mSemicolonLeaf));
	}
	//read ( identifier INDEX);
	else if (mToken->getLangType() == READ) {
		statementsTree->mStatementTree = new StatementReadTree();
		STATEMENT(statementsTree->mStatementTree);
		SEMICOLONSTM(&(statementsTree->mSemicolonLeaf));
	}
	//{STATEMENTS}
	else if (mToken->getSignType() == CURLY_BRACKET_LEFT) {
		statementsTree->mStatementTree = new StatementStatementsTree();
		STATEMENT(statementsTree->mStatementTree);
		SEMICOLONSTM(&(statementsTree->mSemicolonLeaf));
	}
	//if ( EXP ) STATEMENT else STATEMENT -> beide stmts hier mit ; beenden
	else if (mToken->getLangType() == IF) {
		statementsTree->mStatementTree = new StatementIfElseTree();
		STATEMENT(statementsTree->mStatementTree);
		SEMICOLONSTM(&(statementsTree->mSemicolonLeaf));
	}
	//while ( EXP ) STATEMENT
	else if (mToken->getLangType() == WHILE) {
		statementsTree->mStatementTree = new StatementWhileTree();
		STATEMENT(statementsTree->mStatementTree);
		SEMICOLONSTM(&(statementsTree->mSemicolonLeaf));
	}
	else if (mToken->getLangType() == NO_TOKEN) {
		return;
	}
	else {
		return;
	}

	//If there are more statements call STATEMENTS
	if (hasStatements()) {
		statementsTree->mStatementsTree = new StatementsTree();
		STATEMENTS(statementsTree->mStatementsTree);
	}
	else {
		statementsTree->mStatementsTree = new StatementsTree();
	}
}

void Parser::SEMICOLONSTM(Leaf** leaf) {
	if (mToken->getSignType() == SEMICOLON) {
		*leaf = new Leaf(mToken);
		nextToken();
	}
	else {
		error();
	}
}

void Parser::STATEMENT(Tree* tree) {
	//identifier
	if (mToken->getLangType() == IDENTIFIER) {
		StatementAssignmentTree* assignTree = dynamic_cast<StatementAssignmentTree*>(tree);
		assignTree->mIdentifierLeaf = new Leaf(mToken);

		nextToken();
		assignTree->mIndexTree = new IndexTree();
		INDEX(assignTree->mIndexTree);

		//identifier :=
		if (mToken->getSignType() == COLON_EQUALS) {
			assignTree->mOpEqualsLeaf = new Leaf(mToken);

			nextToken();
			assignTree->mExpTree = new ExpTree();
			EXP(assignTree->mExpTree);
		}
		else {
			error();
		}
	}
	//write
	else if (mToken->getLangType() == WRITE) {
		StatementWriteTree* writeTree = dynamic_cast<StatementWriteTree*>(tree);
		writeTree->mWriteLeaf = new Leaf(mToken);

		nextToken();
		//write(
		if (mToken->getSignType() == BRACKET_LEFT) {
			writeTree->mLeftBracketLeaf = new Leaf(mToken);

			nextToken();
			writeTree->mExpTree = new ExpTree();
			EXP(writeTree->mExpTree);

			//write(EXP)
			if (mToken->getSignType() == BRACKET_RIGHT) {
				writeTree->mRightBracketLeaf = new Leaf(mToken);
				nextToken();
			}
			else {
				error();
			}
		}
		else {
			error();
		}
	}
	//read
	else if (mToken->getLangType() == READ) {
		StatementReadTree* rTree = dynamic_cast<StatementReadTree*>(tree);
		rTree->mReadLeaf = new Leaf(mToken);

		nextToken();
		//read(
		if (mToken->getSignType() == BRACKET_LEFT) {
			rTree->mLeftBracketLeaf = new Leaf(mToken);
			nextToken();

			//read(identifier
			if (mToken->getLangType() == IDENTIFIER) {
				rTree->mIdentifierLeaf = new Leaf(mToken);

				nextToken();
				rTree->mIndexTree = new IndexTree();
				INDEX(rTree->mIndexTree);

				//read(identifier INDEX)
				if (mToken->getSignType() == BRACKET_RIGHT) {
					rTree->mRichtBracketLeaf = new Leaf(mToken);
					nextToken();
				}
				else {
					error();
				}
			}
			else {
				error();
			}
		}
		else {
			error();
		}
	}
	//{
	else if (mToken->getSignType() == CURLY_BRACKET_LEFT) {
		StatementStatementsTree* sTree = dynamic_cast<StatementStatementsTree*>(tree);
		sTree->mLeftCurlyBracketLeaf = new Leaf(mToken);

		nextToken();
		sTree->mStatementsTree = new StatementsTree();
		STATEMENTS(sTree->mStatementsTree);

		//{STATEMENTS}
		if (mToken->getSignType() == CURLY_BRACKET_RIGHT) {
			sTree->mRightCurlyBracketLeaf = new Leaf(mToken);
			nextToken();
		}
		else {
			error();
		}
	}
	//if
	else if (mToken->getLangType() == IF) {
		StatementIfElseTree* iTree = dynamic_cast<StatementIfElseTree*>(tree);
		iTree->mIfLeaf = new Leaf(mToken);

		nextToken();

		//if(
		if (mToken->getSignType() == BRACKET_LEFT) {
			iTree->mBracketLeftLeaf = new Leaf(mToken);

			nextToken();
			iTree->mExpTree = new ExpTree();
			EXP(iTree->mExpTree);

			//if(EXP)
			if (mToken->getSignType() == BRACKET_RIGHT) {
				iTree->mBracketRightLeaf = new Leaf(mToken);

				nextToken();
				iTree->mIfStatementTree = getStatementInstance();
				STATEMENT(iTree->mIfStatementTree);

				//if(EXP) STATEMENT else
				if (mToken->getLangType() == ELSE) {
					iTree->mElseLeaf = new Leaf(mToken);

					nextToken();
					iTree->mElseStatementTree = getStatementInstance();
					STATEMENT(iTree->mElseStatementTree);
				}
				else {
					error();
				}
			}
			else {
				error();
			}
		}
		else {
			error();
		}
	}
	//while
	else if (mToken->getLangType() == WHILE) {
		StatementWhileTree* wTree = dynamic_cast<StatementWhileTree*>(tree);
		wTree->mWhileLeaf = new Leaf(mToken);

		nextToken();

		//while(
		if (mToken->getSignType() == BRACKET_LEFT) {
			wTree->mBracketLeftLeaf = new Leaf(mToken);

			nextToken();
			wTree->mExpTree = new ExpTree();
			EXP(wTree->mExpTree);

			//while(EXP)
			if (mToken->getSignType() == BRACKET_RIGHT) {
				wTree->mBracketRightLeaf = new Leaf(mToken);

				nextToken();
				wTree->mStatementTree = getStatementInstance();
				STATEMENT(wTree->mStatementTree);
			}
			else {
				error();
			}
		}
		else {
			error();
		}
	}
}

void Parser::EXP(ExpTree* expTree) {

	expTree->mExp2Tree = getExp2Instance();
	EXP2(expTree->mExp2Tree);

	expTree->mOp_ExpTree = new OpExpTree();
	OP_EXP(expTree->mOp_ExpTree);
}

void Parser::EXP2(Tree* tree) {
	//(
	if (mToken->getSignType() == BRACKET_LEFT) {
		Exp2BracketTree* bTree = dynamic_cast<Exp2BracketTree*>(tree);
		bTree->mLeftBracketLeaf = new Leaf(mToken);

		nextToken();

		bTree->mExpTree = new ExpTree();
		EXP(bTree->mExpTree);

		//(EXP)
		if (mToken->getSignType() == BRACKET_RIGHT) {
			bTree->mRightLeaf = new Leaf(mToken);
			nextToken();
		}
		else {
			error();
		}
	}
	//identifier
	else if (mToken->getLangType() == IDENTIFIER) {
		Exp2IdentifierTree* iTree = dynamic_cast<Exp2IdentifierTree*>(tree);
		iTree->mIdentifierLeaf = new Leaf(mToken);

		nextToken();
		iTree->mIndexTree = new IndexTree();
		INDEX(iTree->mIndexTree);
	}
	//integer
	else if (mToken->getLangType() == INTEGER) {
		Exp2IntegerTree* iTree = dynamic_cast<Exp2IntegerTree*>(tree);
		iTree->mIntegerLeaf = new Leaf(mToken);
		nextToken();
	}
	else if (mToken->getSignType() == MINUS) {
		Exp2MinusExp2Tree* mTree = dynamic_cast<Exp2MinusExp2Tree*>(tree);
		mTree->mMinusLeaf = new Leaf(mToken);

		nextToken();
		mTree->mExp2Tree = getExp2Instance();
		EXP2(mTree->mExp2Tree);
	}
	else if (mToken->getSignType() == EXCLAMATION) {
		Exp2ExclamationExp2Tree *eTree = dynamic_cast<Exp2ExclamationExp2Tree*>(tree);
		eTree->mExclamationLeaf = new Leaf(mToken);

		nextToken();
		eTree->mExp2Tree = getExp2Instance();
		EXP2(eTree->mExp2Tree);
	}
	else {
		error();
	}
}

void Parser::INDEX(IndexTree* indexTree) {

	//[
	if (mToken->getSignType() == SQUARE_BRACKET_LEFT) {
		indexTree->mLeftCurlyBracketLeaf = new Leaf(mToken);
		nextToken();

		indexTree->mExpTree = new ExpTree();
		EXP(indexTree->mExpTree);

		//[EXP]
		if (mToken->getSignType() == SQUARE_BRACKET_RIGHT) {
			indexTree->mRightCurlyBracketLeaf = new Leaf(mToken);
			nextToken();
		}
		else {
			error();
		}
	}
}

void Parser::OP_EXP(OpExpTree* opExpTree) {

	if (mToken->getSignType() == PLUS
			|| mToken->getSignType() == MINUS
			|| mToken->getSignType() == STAR
			|| mToken->getSignType() == COLON
			|| mToken->getSignType() == LEFT_ARROW
			|| mToken->getSignType() == RIGHT_ARROW
			|| mToken->getSignType() == EQUALS
			|| mToken->getSignType() == LEFT_ARROW_COLON_RIGHT_ARROW
			|| mToken->getSignType() == AND) {

		opExpTree->mOpTree = new OpTree();
		OP(opExpTree->mOpTree);

		opExpTree->mExpTree = new ExpTree();
		EXP(opExpTree->mExpTree);
	}
}

void Parser::OP(OpTree* opTree) {
	if (mToken->getSignType() == PLUS
			|| mToken->getSignType() == MINUS
			|| mToken->getSignType() == STAR
			|| mToken->getSignType() == COLON
			|| mToken->getSignType() == LEFT_ARROW
			|| mToken->getSignType() == RIGHT_ARROW
			|| mToken->getSignType() == EQUALS
			|| mToken->getSignType() == LEFT_ARROW_COLON_RIGHT_ARROW
			|| mToken->getSignType() == AND) {

			opTree->mOperatorLeaf = new Leaf(mToken);
			nextToken();
	}
	else {
		error();
	}
}

bool Parser::nextToken() {
	//delete mToken;

	if (mToken) {
		mLastToken = mToken;
	}

	mToken = mScanner->nextToken();
	if (mToken) {
		return true;
	}
	else {
		mToken = new Token(NO_TOKEN, 0, 0, 0, 0);
		return false;
	}
}

bool Parser::hasStatements() {
	//identifier INDEX := EXP;
	if (mToken->getLangType() == IDENTIFIER) {
		return true;
	}
	//write( EXP );
	else if (mToken->getLangType() == WRITE) {
		return true;
	}
	//read ( identifier INDEX);
	else if (mToken->getLangType() == READ) {
		return true;
	}
	//{STATEMENTS}
	else if (mToken->getSignType() == CURLY_BRACKET_LEFT) {
		return true;
	}
	//if ( EXP ) STATEMENT else STATEMENT -> beide stmts hier mit ; beenden
	else if (mToken->getLangType() == IF) {
		return true;
	}
	//while ( EXP ) STATEMENT
	else if (mToken->getLangType() == WHILE) {
		return true;
	}
	else if (mToken->getLangType() == NO_TOKEN) {
		return false;
	}

	return false;
}

Tree* Parser::getExp2Instance() {
	if (mToken->getSignType() == BRACKET_LEFT) {
		return new Exp2BracketTree();
	}
	//identifier
	else if (mToken->getLangType() == IDENTIFIER) {
		return new Exp2IdentifierTree();
	}
	//integer
	else if (mToken->getLangType() == INTEGER) {
		return new Exp2IntegerTree();
	}
	else if (mToken->getSignType() == MINUS) {
		return new Exp2MinusExp2Tree();
	}
	else if (mToken->getSignType() == EXCLAMATION) {
		return new Exp2ExclamationExp2Tree();
	}
	else {
		return 0;
	}
}

Tree* Parser::getStatementInstance() {
	//identifier INDEX := EXP;
	if (mToken->getLangType() == IDENTIFIER) {
		return new StatementAssignmentTree();
	}
	//write( EXP );
	else if (mToken->getLangType() == WRITE) {
		return new StatementWriteTree();
	}
	//read ( identifier INDEX);
	else if (mToken->getLangType() == READ) {
		return new StatementReadTree();
	}
	//{STATEMENTS}
	else if (mToken->getSignType() == CURLY_BRACKET_LEFT) {
		return new StatementStatementsTree();
	}
	//if ( EXP ) STATEMENT else STATEMENT
	else if (mToken->getLangType() == IF) {
		return new StatementIfElseTree();
	}
	//while ( EXP ) STATEMENT
	else if (mToken->getLangType() == WHILE) {
		return new StatementWhileTree();
	}
	else if (mToken->getLangType() == NO_TOKEN) {
		return 0;
	}
}
