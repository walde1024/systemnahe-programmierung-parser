# Definition der Variablen

# enthaelt die Header Files
HEADERDIR = includes

# enthaelt die Source Files
SRCDIR = src

# enthaelt die Obj Files zum Testen
OBJDIR = objs

# enthaelt das ausfuehrbare File zum Testen
BINDIRTEST = debug


## hier alle Object Files eintragen, ausser das File mit der Mainmethode
## darauf achten, dass letzte Zeile ohne "\" eingetragen wird

OBJS = $(OBJDIR)/Parser.o \
	$(OBJDIR)/Client.o \
	$(OBJDIR)/tree/Tree.o \
	$(OBJDIR)/tree/Leaf.o \
	$(OBJDIR)/Type.o \
	$(OBJDIR)/tree/ArrayTree.o \
	$(OBJDIR)/tree/DeclsTree.o \
	$(OBJDIR)/tree/DeclTree.o \
	$(OBJDIR)/tree/Exp2BracketTree.o \
	$(OBJDIR)/tree/Exp2ExclamationExp2Tree.o \
	$(OBJDIR)/tree/Exp2IdentifierTree.o \
	$(OBJDIR)/tree/Exp2IntegerTree.o \
	$(OBJDIR)/tree/Exp2MinusExp2Tree.o \
	$(OBJDIR)/tree/ExpTree.o \
	$(OBJDIR)/tree/IndexTree.o \
	$(OBJDIR)/tree/OpExpTree.o \
	$(OBJDIR)/tree/OpTree.o \
	$(OBJDIR)/tree/ProgTree.o \
	$(OBJDIR)/tree/StatementAssignmentTree.o \
	$(OBJDIR)/tree/StatementIfElseTree.o \
	$(OBJDIR)/tree/StatementReadTree.o \
	$(OBJDIR)/tree/StatementStatementsTree.o \
	$(OBJDIR)/tree/StatementsTree.o \
	$(OBJDIR)/tree/StatementWhileTree.o \
	$(OBJDIR)/tree/StatementWriteTree.o \
	#$(OBJDIR)/xx1.o \
	#$(OBJDIR)/xxn.o
	
# Variante mit guten Makefile-Kenntnissen
makeParser: $(OBJS) Test


$(OBJDIR)/%.o :  $(SRCDIR)/%.cpp $(HEADERDIR)/%.h
	@echo "g++ $*.cpp"
	g++ -g  -c -Wall -o $@ $<



# 
# $@ name des object files
# $< erste abhaengigkeit


Test: $(SRCDIR)/TestParser.cpp $(HEADERDIR)/Parser.h
	g++ -g  -c -Wall src/TestParser.cpp -o $(BINDIRTEST)/TestParser.o 
	g++ -g  $(BINDIRTEST)/TestParser.o $(OBJDIR)/tree/*.o $(OBJDIR)/*.o -o $(BINDIRTEST)/Test -Llib -lSymboltable -lScanner 
	

# loeschen aller files im verzeichnis $(OBJDIR) und $(BINDIRTEST) und neu erstellen

cleanParser:
	rm -rf $(OBJDIR)/*.o
	rm -rf $(OBJDIR)/tree/*.o
	rm -rf $(BINDIRTEST)/*
	$(MAKE) makeParser
	
all:
	$(MAKE) makeParser

#use ar crf libbS.a *.o to generate library
