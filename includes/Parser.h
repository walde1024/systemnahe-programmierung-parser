/*
 * Parser.h
 *
 *  Created on: Oct 27, 2015
 *      Author: walde
 */

#ifndef INCLUDES_PARSER_H_
#define INCLUDES_PARSER_H_

class Scanner;
class FileWriter;
class Symboltable;
class Token;
class DeclsTree;
class DeclTree;
class ArrayTree;
class StatementsTree;
class Tree;
class IndexTree;
class OpExpTree;
class ExpTree;
class OpTree;
class ProgTree;
class Leaf;

class Symboltable;

class Parser {
private:

	Scanner* mScanner;
	Symboltable *mTable;
	FileWriter *mWriter;

	Token* mToken;
	Token* mLastToken;

	bool mErrorWhileParsing;

public:
	Parser(char *inputFile, char *outputFile);
	virtual ~Parser();

	Symboltable* getSymboltable();

	//Returns false if no syntax errors were detected
	bool parse(ProgTree* tree);

	//True if next token exists
	bool nextToken();

	void error();

	void PROG(ProgTree* tree);
	void DECLS(DeclsTree* declsTree);
	void STATEMENTS(StatementsTree* statementsTree);

	void DECL(DeclTree* declTree);
	void ARRAY(ArrayTree* arrayTree);

	void STATEMENT(Tree* tree);
	void EXP(ExpTree* expTree);
	void EXP2(Tree* tree);
	void INDEX(IndexTree* indexTree);
	void OP_EXP(OpExpTree* opExpTree);
	void OP(OpTree* opTree);

	void SEMICOLONSTM(Leaf** leaf);
	bool hasStatements();

	//helper
	Tree* getExp2Instance();
	Tree* getStatementInstance();
};



#endif /* INCLUDES_PARSER_H_ */
