/*
 * Client.h
 *
 *  Created on: Oct 29, 2015
 *      Author: walde
 */

#ifndef SRC_CLIENT_H_
#define SRC_CLIENT_H_

class Parser;
class FileWriter;

class Client {
private:

	Parser* mParser;
	FileWriter* mCodeFileWriter;

public:
	Client(char *inputFile, char *outputFile);
	virtual ~Client();

	void parse();
};

#endif /* SRC_CLIENT_H_ */
