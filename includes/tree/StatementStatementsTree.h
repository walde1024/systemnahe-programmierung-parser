/*
 * StatementStatementsTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_STATEMENTSTATEMENTSTREE_H_
#define INCLUDES_TREE_STATEMENTSTATEMENTSTREE_H_

class Leaf;
class StatementsTree;
class FileWriter;

#include "Tree.h"

class StatementStatementsTree: public Tree {
public:
	StatementStatementsTree();
	virtual ~StatementStatementsTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//{STATEMENTS}
	Leaf* mLeftCurlyBracketLeaf=0;
	StatementsTree* mStatementsTree=0;
	Leaf* mRightCurlyBracketLeaf=0;
};

#endif /* INCLUDES_TREE_STATEMENTSTATEMENTSTREE_H_ */
