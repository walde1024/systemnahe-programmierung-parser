/*
 * Leaf.h
 *
 *  Created on: Nov 5, 2015
 *      Author: walde
 */

#ifndef SRC_LEAF_H_
#define SRC_LEAF_H_

class Token;
class FileWriter;

#include "Tree.h"

class Leaf: Tree{
private:

	Token* mToken;

public:
	Leaf(Token* token);
	virtual ~Leaf();

	Token* getToken();
	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);
};

#endif /* SRC_LEAF_H_ */
