/*
 * Exp2Integer.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_EXP2INTEGERTREE_H_
#define INCLUDES_TREE_EXP2INTEGERTREE_H_

class Leaf;
class FileWriter;

#include "Tree.h"

class Exp2IntegerTree: public Tree {
public:
	Exp2IntegerTree();
	virtual ~Exp2IntegerTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//integer (Number - not the keyword)
	Leaf* mIntegerLeaf=0;
};

#endif /* INCLUDES_TREE_EXP2INTEGERTREE_H_ */
