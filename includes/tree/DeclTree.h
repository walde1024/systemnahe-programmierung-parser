/*
 * DeclTree.h
 *
 *  Created on: Nov 6, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_DECLTREE_H_
#define INCLUDES_TREE_DECLTREE_H_

class Leaf;
class ArrayTree;
class FileWriter;

#include "Tree.h"

class DeclTree: public Tree {
public:
	DeclTree();
	virtual ~DeclTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//int ARRAY identifier
	Leaf* mIntLeaf=0;
	ArrayTree* mArrayTree=0;
	Leaf* mIdentifierLeaf=0;
};

#endif /* INCLUDES_TREE_DECLTREE_H_ */
