/*
 * Exp2ExclamationExp2Tree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_EXP2EXCLAMATIONEXP2TREE_H_
#define INCLUDES_TREE_EXP2EXCLAMATIONEXP2TREE_H_

class Leaf;
class FileWriter;

#include "Tree.h"

class Exp2ExclamationExp2Tree: public Tree {
public:
	Exp2ExclamationExp2Tree();
	virtual ~Exp2ExclamationExp2Tree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//! EXP2
	Leaf* mExclamationLeaf=0;
	Tree* mExp2Tree=0;
};

#endif /* INCLUDES_TREE_EXP2EXCLAMATIONEXP2TREE_H_ */
