/*
 * OpTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_OPTREE_H_
#define INCLUDES_TREE_OPTREE_H_

class Leaf;
class FileWriter;

#include "Tree.h"

class OpTree: public Tree {
public:
	OpTree();
	virtual ~OpTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//Operator
	Leaf* mOperatorLeaf=0;
};

#endif /* INCLUDES_TREE_OPTREE_H_ */
