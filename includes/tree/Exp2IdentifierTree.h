/*
 * Exp2Identifier.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_EXP2IDENTIFIERTREE_H_
#define INCLUDES_TREE_EXP2IDENTIFIERTREE_H_

class Leaf;
class IndexTree;
class FileWriter;

#include "Tree.h"

class Exp2IdentifierTree: public Tree {
public:
	Exp2IdentifierTree();
	virtual ~Exp2IdentifierTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//identifier INDEX
	Leaf* mIdentifierLeaf=0;
	IndexTree* mIndexTree=0;
};

#endif /* INCLUDES_TREE_EXP2IDENTIFIERTREE_H_ */
