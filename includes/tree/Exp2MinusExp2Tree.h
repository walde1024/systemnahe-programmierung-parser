/*
 * Exp2MinusExp2Tree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_EXP2MINUSEXP2TREE_H_
#define INCLUDES_TREE_EXP2MINUSEXP2TREE_H_

class Leaf;
class FileWriter;

#include "Tree.h"

class Exp2MinusExp2Tree: public Tree {
public:
	Exp2MinusExp2Tree();
	virtual ~Exp2MinusExp2Tree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//- EXP2
	Leaf* mMinusLeaf=0;
	Tree* mExp2Tree=0;
};

#endif /* INCLUDES_TREE_EXP2MINUSEXP2TREE_H_ */
