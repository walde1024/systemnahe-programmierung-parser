/*
 * ArrayTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_ARRAYTREE_H_
#define INCLUDES_TREE_ARRAYTREE_H_

class Leaf;
class FileWriter;

#include "Tree.h"

class ArrayTree: public Tree {
public:
	ArrayTree();
	virtual ~ArrayTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//[integer] | e
	Leaf* mLeftSquareBracketLeaf=0;
	Leaf* mIntegerLeaf=0;
	Leaf* mRightSquareBracketLeaf=0;
};

#endif /* INCLUDES_TREE_ARRAYTREE_H_ */
