/*
 * StatementReadTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_STATEMENTREADTREE_H_
#define INCLUDES_TREE_STATEMENTREADTREE_H_

class Leaf;
class IndexTree;
class FileWriter;

#include "Tree.h"

class StatementReadTree: public Tree {
public:
	StatementReadTree();
	virtual ~StatementReadTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//read ( identifier INDEX )
	Leaf* mReadLeaf=0;
	Leaf* mLeftBracketLeaf=0;
	Leaf* mIdentifierLeaf=0;
	IndexTree* mIndexTree=0;
	Leaf* mRichtBracketLeaf=0;
};

#endif /* INCLUDES_TREE_STATEMENTREADTREE_H_ */
