/*
 * OpExpTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_OPEXPTREE_H_
#define INCLUDES_TREE_OPEXPTREE_H_

class OpTree;
class ExpTree;
class FileWriter;

#include "Tree.h"

class OpExpTree: public Tree {
public:
	OpExpTree();
	virtual ~OpExpTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//OP EXP | e
	OpTree* mOpTree=0;
	ExpTree* mExpTree=0;
};

#endif /* INCLUDES_TREE_OPEXPTREE_H_ */
