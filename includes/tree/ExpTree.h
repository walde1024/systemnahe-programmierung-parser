/*
 * ExpTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_EXPTREE_H_
#define INCLUDES_TREE_EXPTREE_H_

class OpExpTree;
class FileWriter;

#include "Tree.h"

class ExpTree: public Tree {
public:
	ExpTree();
	virtual ~ExpTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//EXP2 OP_EXP
	Tree* mExp2Tree=0;
	OpExpTree* mOp_ExpTree=0;
};

#endif /* INCLUDES_TREE_EXPTREE_H_ */
