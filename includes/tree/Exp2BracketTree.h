/*
 * Exp2BracketTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_EXP2BRACKETTREE_H_
#define INCLUDES_TREE_EXP2BRACKETTREE_H_

class Leaf;
class ExpTree;
class FileWriter;

#include "Tree.h"

class Exp2BracketTree: public Tree {
public:
	Exp2BracketTree();
	virtual ~Exp2BracketTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//(EXP)
	Leaf* mLeftBracketLeaf=0;
	ExpTree* mExpTree=0;
	Leaf* mRightLeaf=0;
};

#endif /* INCLUDES_TREE_EXP2BRACKETTREE_H_ */
