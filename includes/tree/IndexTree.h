/*
 * IndexTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_INDEXTREE_H_
#define INCLUDES_TREE_INDEXTREE_H_

class Leaf;
class ExpTree;
class FileWriter;

#include "Tree.h"

class IndexTree: public Tree {
public:
	IndexTree();
	virtual ~IndexTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//[EXP] | e
	Leaf* mLeftCurlyBracketLeaf=0;
	ExpTree* mExpTree=0;
	Leaf* mRightCurlyBracketLeaf=0;
};

#endif /* INCLUDES_TREE_INDEXTREE_H_ */
