/*
 * DeclsTree.h
 *
 *  Created on: Nov 6, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_DECLSTREE_H_
#define INCLUDES_TREE_DECLSTREE_H_

class DeclTree;
class Leaf;
class FileWriter;

#include "Tree.h"

class DeclsTree: public Tree {
public:
	DeclsTree();
	virtual ~DeclsTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	DeclTree* mDeclTree=0;
	Leaf* mSemiliconLeaf=0;
	DeclsTree* mDeclsTree=0;
};

#endif /* INCLUDES_TREE_DECLSTREE_H_ */
