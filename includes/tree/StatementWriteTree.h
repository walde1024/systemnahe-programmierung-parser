/*
 * StatementWriteTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_STATEMENTWRITETREE_H_
#define INCLUDES_TREE_STATEMENTWRITETREE_H_

class Leaf;
class ExpTree;
class FileWriter;

#include "Tree.h"

class StatementWriteTree: public Tree {
public:
	StatementWriteTree();
	virtual ~StatementWriteTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//write( EXP )
	Leaf* mWriteLeaf=0;
	Leaf* mLeftBracketLeaf=0;
	ExpTree* mExpTree=0;
	Leaf* mRightBracketLeaf=0;
};

#endif /* INCLUDES_TREE_STATEMENTWRITETREE_H_ */
