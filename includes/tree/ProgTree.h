/*
 * ProgTree.h
 *
 *  Created on: Nov 6, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_PROGTREE_H_
#define INCLUDES_TREE_PROGTREE_H_

class DeclsTree;
class StatementsTree;
class FileWriter;

#include "Tree.h"

class ProgTree: public Tree {
public:
	ProgTree();
	virtual ~ProgTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	DeclsTree* mDeclsTree=0;
	StatementsTree* mStatementsTree=0;
};

#endif /* INCLUDES_TREE_PROGTREE_H_ */
