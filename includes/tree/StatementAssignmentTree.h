/*
 * StatementAssignmentTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_STATEMENTASSIGNMENTTREE_H_
#define INCLUDES_TREE_STATEMENTASSIGNMENTTREE_H_

class Leaf;
class IndexTree;
class ExpTree;
class FileWriter;

#include "Tree.h"

class StatementAssignmentTree: public Tree {
public:
	StatementAssignmentTree();
	virtual ~StatementAssignmentTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//identifier INDEX := EXP
	Leaf* mIdentifierLeaf=0;
	IndexTree* mIndexTree=0;
	Leaf* mOpEqualsLeaf=0;
	ExpTree* mExpTree=0;
};

#endif /* INCLUDES_TREE_STATEMENTASSIGNMENTTREE_H_ */
