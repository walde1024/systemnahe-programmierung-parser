/*
 * StatementIfElseTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_STATEMENTIFELSETREE_H_
#define INCLUDES_TREE_STATEMENTIFELSETREE_H_

class Leaf;
class ExpTree;
class FileWriter;

#include "Tree.h"

class StatementIfElseTree: public Tree {
public:
	StatementIfElseTree();
	virtual ~StatementIfElseTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//if ( EXP ) STATEMENT else STATEMENT
	Leaf* mIfLeaf=0;
	Leaf* mBracketLeftLeaf=0;
	ExpTree* mExpTree=0;
	Leaf* mBracketRightLeaf=0;
	Tree* mIfStatementTree=0;
	Leaf* mElseLeaf=0;
	Tree* mElseStatementTree=0;
};

#endif /* INCLUDES_TREE_STATEMENTIFELSETREE_H_ */
