/*
 * Tree.h
 *
 *  Created on: Nov 5, 2015
 *      Author: walde
 */

#ifndef SRC_TREE_H_
#define SRC_TREE_H_

class Symboltable;
class Leaf;
class Token;
class FileWriter;

#include "../Type.h"

class Tree {
private:

	Type mType;
	static bool mHasTypeCheckError;
	void error(char message);

	char* mCharBuffer;

	static int mCurrentLabelNumber;

public:
	Tree(Type type);
	virtual ~Tree();

	char* const mIntTypeValue = "I";
	char* const mArrayTypeValue = "A";

	virtual void typeCheck(Symboltable* table)=0;
	virtual void makeCode(FileWriter* writer, Symboltable* table)=0;

	virtual Type getType();
	virtual void setType(Type type);

	void writeNewLine(FileWriter* writer);

	bool hasTypeCheckError();

	//generated a key for a lexem to store the identifier type with following pattern: "type<lexem>"
	//assigns the created key to the delivered key pointer.
	int createTypeKey(char** key, Leaf* leaf, Symboltable* table);

	//Returns the typeVale from the symboltable or null
	char* getTypeValue(Symboltable* table, Leaf* leaf);

	char* parseIntToCharPointer(int value);

	int getCurrentLabelNumber();

	void errorPrintToken(Token* token);
	void errorIdentifierAlreadyDefined(Token* token);
	void errorIdentifierNotDefined(Token* token);
	void errorIncompatibleTypes(Token* token);
	void errorNoPrimitiveType(Token* token);
	void errorArraySizeIsZero(Token* token);
};

#endif /* SRC_TREE_H_ */
