/*
 * StatementWhileTree.h
 *
 *  Created on: Nov 7, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_STATEMENTWHILETREE_H_
#define INCLUDES_TREE_STATEMENTWHILETREE_H_

class Leaf;
class ExpTree;
class FileWriter;

#include "Tree.h"

class StatementWhileTree: public Tree {
public:
	StatementWhileTree();
	virtual ~StatementWhileTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	//while ( EXP ) STATEMENT
	Leaf* mWhileLeaf=0;
	Leaf* mBracketLeftLeaf=0;
	ExpTree* mExpTree=0;
	Leaf* mBracketRightLeaf=0;
	Tree* mStatementTree=0;
};

#endif /* INCLUDES_TREE_STATEMENTWHILETREE_H_ */
