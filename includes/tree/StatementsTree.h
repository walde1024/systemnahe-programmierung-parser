/*
 * StatementsTree.h
 *
 *  Created on: Nov 6, 2015
 *      Author: walde
 */

#ifndef INCLUDES_TREE_STATEMENTSTREE_H_
#define INCLUDES_TREE_STATEMENTSTREE_H_

class Leaf;
class FileWriter;

#include "Tree.h"

class StatementsTree: public Tree {
public:
	StatementsTree();
	virtual ~StatementsTree();

	virtual void typeCheck(Symboltable* table);
	virtual void makeCode(FileWriter* writer, Symboltable* table);

	Tree* mStatementTree=0;
	Leaf* mSemicolonLeaf=0;
	StatementsTree* mStatementsTree=0;
};

#endif /* INCLUDES_TREE_STATEMENTSTREE_H_ */
