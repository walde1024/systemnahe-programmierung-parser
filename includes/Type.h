#ifndef TYPE_H
#define TYPE_H

enum Type {
	intType , intArrayType, arrayType, noType, errorType, emptyType,
	opPlus, opMinus, opMult, opDiv, opLess, opGreater, opEqual, opUnEqual, opAnd
};

#endif /* TYPE_H */
